//
//  INSNanoSDK.h
//  INSNanoSDK
//
//  Created by pwx on 9/3/16.
//  Copyright © 2016年 com.insta360. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for INSNanoSDK.
FOUNDATION_EXPORT double INSNanoSDKVersionNumber;

//! Project version string for INSNanoSDK.
FOUNDATION_EXPORT const unsigned char INSNanoSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <INSNanoSDK/PublicHeader.h>


#import <INSNanoSDK/INSCameraDef.h>
#import <INSNanoSDK/INSCameraAccessory.h>
#import <INSNanoSDK/INSLiveDataSource.h>
#import <INSNanoSDK/INSLiveStreamer.h>
#import <INSNanoSDK/INSCameraVideoResType.h>
#import <INSNanoSDK/INSFlatLiveStreamer.h>
