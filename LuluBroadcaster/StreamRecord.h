//
//  StreamRecord.h
//  LuluBroadcaster
//
//  Created by 林威 on 17/6/19.
//  Copyright © 2017年 ShuoTan. All rights reserved.
//

#import "Record.h"

@interface StreamRecord : Record

@property(nonatomic, strong, nonnull) NSString* date;
@property(nonatomic, strong, nonnull) NSString* time;
@property(nonatomic, strong, nonnull) NSString* cost_wealth;
@property(nonatomic, strong, nonnull) NSString* room;

+ (StreamRecord* _Nonnull)streamRecordWithJSON:(id _Nonnull)data;

@end
