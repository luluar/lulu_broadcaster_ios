//
//  Record.m
//  LuluBroadcaster
//
//  Created by 林威 on 17/6/19.
//  Copyright © 2017年 ShuoTan. All rights reserved.
//

#import "Record.h"
#import "LiveRecord.h"
#import "GiftRecord.h"
#import "StreamRecord.h"

typedef NS_ENUM(NSInteger, TypeOfRecord) {
    RecordOfPlay                      = 0,
    RecordOfGift                      = 1,
    RecordOfFlow                      = 2,
    RecordOfBunko                     = 3
};

@implementation Record

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        
    }
    return self;
}

+ (Record* _Nonnull) recordWithJSON:(id _Nonnull)data type:(NSInteger)type
{
    Record* record = nil;
    switch (type)
    {
        case RecordOfPlay:
            record = [LiveRecord liveRecordWithJSON:data];
            break;
        case RecordOfGift:
            record = [GiftRecord giftRecordWithJSON:data];
            break;
        case RecordOfFlow:
            record = [StreamRecord streamRecordWithJSON:data];
            break;
        case RecordOfBunko:
            record = [StreamRecord streamRecordWithJSON:data];
            break;
    }
    return record;
}

@end
