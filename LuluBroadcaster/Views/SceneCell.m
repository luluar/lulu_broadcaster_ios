//
//  SceneCell.m
//  LuluBroadcaster
//
//  Created by ShuoTan on 4/26/17.
//  Copyright © 2017 ShuoTan. All rights reserved.
//

#import "SceneCell.h"
#import "LiveRecord.h"
#import "StreamRecord.h"
#import "GiftRecord.h"

@implementation SceneCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = [UIColor clearColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#warning --当前方法工期紧，暂时利用，后期需改造cell层，启用工厂模式
- (void)configureWithRecord: (Record* _Nullable)record{
    NSString *modelName = [NSString stringWithUTF8String:object_getClassName(record)];
    
    if ([modelName isEqualToString:@"LiveRecord"]) {
        
        self.iconImage.alpha = 0;
        self.countLabel.alpha = 0;
        self.durationLabel.alpha = 1;
        
        LiveRecord *liveRecord = (LiveRecord*)record;
        self.dateLabel.text = liveRecord.date;
        self.timeLabel.text = [NSString stringWithFormat:@"%@--%@", liveRecord.start_time, liveRecord.end_time];
        self.durationLabel.text = liveRecord.duration;
        
    }else if ([modelName isEqualToString:@"StreamRecord"]){
        
        self.iconImage.alpha = 1;
        self.countLabel.alpha = 1;
        self.durationLabel.alpha = 0;
        
        StreamRecord *streamRecord = (StreamRecord*)record;
        self.dateLabel.text = streamRecord.date;
        self.timeLabel.text = [NSString stringWithFormat:@"%@", streamRecord.time];
        self.countLabel.text = streamRecord.cost_wealth;
        
    }else if ([modelName isEqualToString:@"GiftRecord"]){
        
        self.iconImage.alpha = 1;
        self.countLabel.alpha = 1;
        self.durationLabel.alpha = 0;
        
        GiftRecord *giftRecord = (GiftRecord*)record;
        self.dateLabel.text = giftRecord.date;
        self.timeLabel.text = [NSString stringWithFormat:@"%@", giftRecord.gift_name];
        self.countLabel.text = giftRecord.gift_cost;
        
    }
    
}

@end
