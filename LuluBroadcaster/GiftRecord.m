//
//  GiftRecord.m
//  LuluBroadcaster
//
//  Created by 林威 on 17/6/19.
//  Copyright © 2017年 ShuoTan. All rights reserved.
//

#import "GiftRecord.h"
#import <KZPropertyMapper/KZPropertyMapper.h>

@implementation GiftRecord

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.date = @"";
        self.gift_name = @"";
        self.gift_cost = @"";
        self.room = @"";
    }
    return self;
}

+ (GiftRecord* _Nonnull) giftRecordWithJSON:(id _Nonnull)data{
    GiftRecord* record = [[GiftRecord alloc] init];
    [record doMappingWithData:data];
    return record;
}

- (void)doMappingWithData: (id)data{
    NSDictionary* mapping = @{@"date": KZProperty(date),
                              @"gift_name": KZProperty(gift_name),
                              @"gift_cost": KZProperty(gift_cost),
                              @"room": KZProperty(room)
                              };
    
    [KZPropertyMapper mapValuesFrom:data toInstance:self usingMapping:mapping];
}

@end
