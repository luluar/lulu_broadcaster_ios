//
//  GameStartAlertView.m
//  LuluBroadcaster
//
//  Created by 林威 on 17/6/22.
//  Copyright © 2017年 ShuoTan. All rights reserved.
//

#import "GameStartAlertView.h"

@interface GameStartAlertView ()

@end

@implementation GameStartAlertView

+ (void)popOutInController: (UIViewController* _Nonnull)controller Message:(NSString* _Nonnull)message error: (NSError* _Nonnull)err cancelHandler:(void (^ __nullable)(UIAlertAction * _Nonnull action))cancelHandler confirmHandler:(void (^ __nullable)(UIAlertAction * _Nonnull action))confirmHandler{
    GameStartAlertView* alertC= [GameStartAlertView alertControllerWithTitle:@"" message:message preferredStyle:UIAlertControllerStyleActionSheet];
    
    [alertC addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:cancelHandler]];
    
    [alertC addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:confirmHandler]];
    
    [controller presentViewController:alertC animated:YES completion:nil];
}

+ (void)dismissGameStartAlertView{
    if ([[GameStartAlertView getCurrentVC]isMemberOfClass:[GameStartAlertView class]]) {
        [[GameStartAlertView getCurrentVC] dismissViewControllerAnimated:NO completion:nil];
    }
}

//获取当前屏幕显示的viewcontroller
+ (UIViewController *)getCurrentVC
{
    UIViewController *result = nil;
    
    UIWindow * window = [[UIApplication sharedApplication] keyWindow];
    if (window.windowLevel != UIWindowLevelNormal)
    {
        NSArray *windows = [[UIApplication sharedApplication] windows];
        for(UIWindow * tmpWin in windows)
        {
            if (tmpWin.windowLevel == UIWindowLevelNormal)
            {
                window = tmpWin;
                break;
            }
        }
    }
    
    UIView *frontView = [[window subviews] objectAtIndex:0];
    id nextResponder = [frontView nextResponder];
    
    if ([nextResponder isKindOfClass:[UIViewController class]])
        result = nextResponder;
    else
        result = window.rootViewController;
    
    return result;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
