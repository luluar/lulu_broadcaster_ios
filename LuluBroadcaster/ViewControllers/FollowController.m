//
//  FollowController.m
//  LuluBroadcaster
//
//  Created by ShuoTan on 4/10/17.
//  Copyright © 2017 ShuoTan. All rights reserved.
//

#import "FollowController.h"
#import "FollowerDatasource.h"
#import "FollowRequest.h"
#import "FollowerCell.h"

@interface FollowController ()<FollowerDatasourceDelegate, UITableViewDelegate, UITableViewDataSource>{
    FollowerDatasource* data;
}
@property (weak, nonatomic) IBOutlet UITableView *table;
@property (weak, nonatomic) IBOutlet UILabel *followersCount;

@end

@implementation FollowController

- (BOOL)shouldAutorotate{
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskAll;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    // Do any additional setup after loading the view from its nib.
    [self setDatasource];
    [self setTable];
    
    
    
    
}

- (void)viewWillAppear:(BOOL)animated{
    [self fetch];
}

- (IBAction)goBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidDisappear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

#pragma mark -
#pragma mark setups

- (void)setDatasource{
    data = [FollowerDatasource new];
    data.delegate = self;
}

- (void)setTable{
    self.table.delegate = self;
    self.table.dataSource = self;
    self.table.backgroundColor = [UIColor clearColor];
    self.table.tableFooterView = [UIView new];
    
    [self.table registerNib:[UINib nibWithNibName:@"FollowerCell" bundle:nil] forCellReuseIdentifier:@"FollowerCellID"];
}

#pragma mark -
#pragma mark methods

- (void)fetch{
    [[FollowRequest sharedRequest] fetchFollowWithCompletion:^(NSArray<User *> * _Nullable followers, NSString* _Nullable followersCount, NSError * _Nullable error) {
        if(followers){
            [data update:followers];
        }
        if (followersCount) {
            _followersCount.text = followersCount;
        }
    }];
}

#pragma mark -
#pragma mark <FollowerDatasourceDelegate>
- (void)dataHasChanged:(NSArray<User *> *)followers{
    [self.table reloadData];
}

#pragma mark -
#pragma mark UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    FollowerCell* cell = [tableView dequeueReusableCellWithIdentifier:@"FollowerCellID"];
    //cell.transaction = [self.datasource getModelAtIndexPath:indexPath];
    [cell configureWithUser:[data getModelAtIndexPath:indexPath]];
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [data numberOfFollowers];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 65.0f;
}

@end
