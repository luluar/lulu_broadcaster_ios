//
//  SceneController.m
//  LuluBroadcaster
//
//  Created by ShuoTan on 4/26/17.
//  Copyright © 2017 ShuoTan. All rights reserved.
//

#import "SceneController.h"
#import "SceneDatasource.h"
#import "SceneRequest.h"
#import "SceneCell.h"
#import <SVPullToRefresh/SVPullToRefresh.h>
#import <SVProgressHUD/SVProgressHUD.h>

typedef NS_ENUM(NSInteger, DateOfRecord) {
    RecordOfToday                     = 0,
    RecordOfThisMonth                 = 1,
    RecordOfLastMonth                 = 2
};

typedef NS_ENUM(NSInteger, TypeOfRecord) {
    RecordOfPlay                      = 0,
    RecordOfGift                      = 1,
    RecordOfFlow                      = 2,
    RecordOfBunko                     = 3
};

@interface SceneController ()<SceneDatasourceDelegate, UITableViewDelegate, UITableViewDataSource>{
    SceneDatasource* data;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *todayBtn;
@property (weak, nonatomic) IBOutlet UIButton *thisMonthBtn;
@property (weak, nonatomic) IBOutlet UIButton *lastMonthBtn;
@property (weak, nonatomic) IBOutlet UILabel *total_durationL;
@property (weak, nonatomic) IBOutlet UIView *typeMenu;
@property (weak, nonatomic) IBOutlet UIButton *typeBtn;
@property (weak, nonatomic) IBOutlet UILabel *totalText;
@property NSInteger date;
@property NSInteger type;
@property NSInteger pageNo;

@end

@implementation SceneController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _todayBtn.selected = YES;
    _date = RecordOfToday;
    _type = RecordOfPlay;
    _pageNo = 0;
    _typeMenu.hidden = YES;
    
    [self fetch];
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    [self setDatasource];
    [self setTable];
}

- (IBAction)goBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidDisappear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

#pragma mark -
#pragma mark setType
- (IBAction)typeAction:(id)sender {
    _typeBtn.hidden = YES;
    _typeMenu.hidden = NO;
}

- (IBAction)recordOfPlay:(id)sender {
    if (_type != RecordOfPlay) {
        _pageNo = 0;
        _type = RecordOfPlay;
        [_typeBtn setTitle:@"直播记录" forState:UIControlStateNormal];
        _typeBtn.hidden = NO;
        _typeMenu.hidden = YES;
        _totalText.text = @"直播时长：";
        [self fetch];
    }
}
- (IBAction)recordOfGift:(id)sender {
    if (_type != RecordOfGift) {
        _pageNo = 0;
        _type = RecordOfGift;
        [_typeBtn setTitle:@"礼物记录" forState:UIControlStateNormal];
        _typeBtn.hidden = NO;
        _typeMenu.hidden = YES;
        _totalText.text = @"礼物汇总：";
        [self fetch];
    }
}
- (IBAction)recordOfFlow:(id)sender {
    if (_type != RecordOfFlow) {
        _pageNo = 0;
        _type = RecordOfFlow;
        [_typeBtn setTitle:@"流水记录" forState:UIControlStateNormal];
        _typeBtn.hidden = NO;
        _typeMenu.hidden = YES;
        _totalText.text = @"流水汇总：";
        [self fetch];
    }
}
- (IBAction)recordOfBunko:(id)sender {
    if (_type != RecordOfBunko) {
        _pageNo = 0;
        _type = RecordOfBunko;
        [_typeBtn setTitle:@"输赢记录" forState:UIControlStateNormal];
        _typeBtn.hidden = NO;
        _typeMenu.hidden = YES;
        _totalText.text = @"输赢汇总：";
        [self fetch];
    }
}

#pragma mark -
#pragma mark setDate
- (IBAction)recordOfToday:(id)sender {
    if (_date != RecordOfToday) {
        _pageNo = 0;
        _date = RecordOfToday;
        [_todayBtn setSelected:YES];
        [_thisMonthBtn setSelected:NO];
        [_lastMonthBtn setSelected:NO];
        [self fetch];
    }
}
- (IBAction)recordOfThisMonth:(id)sender {
    if (_date != RecordOfThisMonth) {
        _pageNo = 0;
        _date = RecordOfThisMonth;
        [_thisMonthBtn setSelected:YES];
        [_todayBtn setSelected:NO];
        [_lastMonthBtn setSelected:NO];
        [self fetch];
    }
}
- (IBAction)recordOfLastMonth:(id)sender {
    if (_date != RecordOfLastMonth) {
        _pageNo = 0;
        _date = RecordOfLastMonth;
        [_lastMonthBtn setSelected:YES];
        [_thisMonthBtn setSelected:NO];
        [_todayBtn setSelected:NO];
        [self fetch];
    }
}

#pragma mark -
#pragma mark setups

- (void)setDatasource{
    data = [SceneDatasource new];
    data.delegate = self;
}

- (void)setTable{
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.tableFooterView = [UIView new];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"SceneCell" bundle:nil] forCellReuseIdentifier:@"SceneCellID"];
    
    
     __weak SceneController* wself = self;
    
    [self.tableView addPullToRefreshWithActionHandler:^{
        wself.pageNo = 0;
        [wself fetch];
    }];
    [_tableView.pullToRefreshView setTitle:@"   松开即可刷新" forState:SVPullToRefreshStateTriggered];
    [_tableView.pullToRefreshView setTitle:@"   继续下拉刷新" forState:SVPullToRefreshStateStopped];
    [_tableView.pullToRefreshView setTitle:@"   不要命的加载中..." forState:SVPullToRefreshStateLoading];
    
    [self.tableView addInfiniteScrollingWithActionHandler:^{
            wself.pageNo++;
            [wself fetch];
    }];
}

//type: integer  //0 直播记录 1 礼物记录 2 流水记录 3 输赢记录
//date: integer  //0 今日 1 本月 2 上月
//pageNo: integer  //页码  0为第一页
- (void)fetch{
    [[SceneRequest sharedRequest] fetchScenesWithType:_type Date:_date PageNo:_pageNo Completion:^(NSArray<Record *> * _Nullable list, NSString * _Nullable total, NSError * _Nullable error, NSInteger isend) {
        if (error) {
            
            [SVProgressHUD showErrorWithStatus:@"请求失败！"];
            return ;
        }
        if (isend == 1) {
            self.tableView.showsInfiniteScrolling = NO;
            [SVProgressHUD showInfoWithStatus:@"已无更多数据"];
        }else{
            self.tableView.showsInfiniteScrolling = YES;
        }
        if(list){
            [data update:list withPage:_pageNo];
        }
        if (total) {
            _total_durationL.text = total;
        }
    }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [data numberOfRecords];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SceneCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SceneCellID" forIndexPath:indexPath];
    
    // Configure the cell...
    [cell configureWithRecord:[data getModelAtIndexPath:indexPath]];
    
    return cell;
}

#pragma mark -
#pragma mark <FollowerDatasourceDelegate>
- (void)dataHasChanged:(NSArray<Record *> *)records{
    [self.tableView reloadData];
//    [self.tableView setContentOffset:CGPointMake(0,0) animated:NO];
    if (_pageNo == 0) {
        [self.tableView.pullToRefreshView stopAnimating];
    }else{
        [self.tableView.infiniteScrollingView stopAnimating];
    }
}
@end
