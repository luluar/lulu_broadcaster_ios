//
//  RankController.m
//  LuluBroadcaster
//
//  Created by ShuoTan on 4/26/17.
//  Copyright © 2017 ShuoTan. All rights reserved.
//

#import "RankController.h"
#import "RankCell.h"

static RankController* _vc = nil;

@interface RankController ()<UITableViewDataSource, UITableViewDelegate>{
}
@property (weak, nonatomic) IBOutlet UITableView *table;
@property (weak, nonatomic) IBOutlet UILabel *totalLabel;
@property (weak, nonatomic) IBOutlet UIButton *resetButton;


@property (assign, nonatomic)NSInteger secondsCoundDown;
@property (strong, nonatomic)NSTimer *countDownTimer;
@property (copy, nonatomic)NSString *time;

@end

@implementation RankController

- (UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskAll;
}

- (BOOL)shouldAutorotate{
    return NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setupTable];
    [self registerClicked];
}

- (void)viewDidAppear:(BOOL)animated{
    [self.table reloadData];
    [self fillInfo];
}

#pragma mark -
#pragma mark setup

- (void)setupTable{
    self.table.delegate = self;
    self.table.dataSource = self;
    self.table.backgroundColor = [UIColor clearColor];
    self.table.tableFooterView = [UIView new];
    
    [self.table registerNib:[UINib nibWithNibName:@"RankCell" bundle:nil] forCellReuseIdentifier:@"RankCellID"];
}

#pragma mark -
#pragma mark methods

- (void)fillInfo{
    NSInteger total = 0;
    for(id iter in _list){
        total -= [iter[@"benifit"] integerValue];
    }
    self.totalLabel.text = [NSString stringWithFormat:@"这回合总共获得： %ld 魔法石", (long)total];
}

+ (void)popoutWithList:(NSArray*)list WithController:(UIViewController*)vc{
    RankController* pop = [[RankController alloc] initWithNibName:@"RankController" bundle:nil];
    pop.list = list;
    _vc = pop;
    pop.view.frame = [UIScreen mainScreen].bounds;
    [pop.view layoutIfNeeded];
    [vc.view addSubview:pop.view];
    [pop viewDidAppear:NO];
}

#pragma mark -
#pragma mark actions
- (IBAction)cancel:(id)sender {
    [_vc.view removeFromSuperview];
    _vc = nil;
}

- (void)registerClicked{
    //设置计时器
    self.secondsCoundDown = 10;
    self.countDownTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timeFireMethod) userInfo:nil repeats:YES];
    //设置按钮不可点击
    [self.resetButton setEnabled:NO];
    [self.resetButton setBackgroundColor:[UIColor grayColor]];
}
//倒计时方法
- (void)timeFireMethod{
    self.secondsCoundDown --;
    //更新按钮倒计时时间
    self.time = [NSMutableString stringWithFormat:@"开始下一局（%ld秒）",(long)self.secondsCoundDown];
    [UIView animateWithDuration:0.5 animations:^{
        [self.resetButton setTitle:self.time forState:UIControlStateDisabled];
    }];
    if (self.secondsCoundDown == 0) {
        [self.countDownTimer invalidate];
        self.countDownTimer = nil;
        //设置按钮可点击
        [self.resetButton setEnabled:YES];
        [self.resetButton setTitle:@"开始下一局" forState:UIControlStateNormal];
        [self.resetButton setBackgroundColor:[UIColor colorWithRed:0/255.0 green:128/255.0 blue:255/255.0 alpha:1]];
    }
}

#pragma mark - 
#pragma mark <UITableViewDataSource, UITableViewDelegate>

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    RankCell* cell = [tableView dequeueReusableCellWithIdentifier:@"RankCellID"];
    [cell configWithData:_list[indexPath.row] WithIndex:indexPath.row];
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.list.count;
}

@end
