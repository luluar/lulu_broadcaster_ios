//
//  LiveController.h
//  LuluBroadcaster
//
//  Created by ShuoTan on 10/31/16.
//  Copyright © 2016 ShuoTan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Scene.h"

@interface LiveController : UIViewController
@property (strong, nonatomic)  Scene* scene;
@end
