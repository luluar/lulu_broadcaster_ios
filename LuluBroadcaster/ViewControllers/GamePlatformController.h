//
//  GamePlatformController.h
//  LuluBroadcaster
//
//  Created by ShuoTan on 12/19/16.
//  Copyright © 2016 ShuoTan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameManager.h"

@interface GamePlatformController : UIViewController
@property (strong, nonatomic)  Scene* scene;
@end
