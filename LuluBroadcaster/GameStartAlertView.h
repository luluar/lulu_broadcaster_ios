//
//  GameStartAlertView.h
//  LuluBroadcaster
//
//  Created by 林威 on 17/6/22.
//  Copyright © 2017年 ShuoTan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GameStartAlertView : UIAlertController
+ (void)popOutInController: (UIViewController* _Nonnull)controller Message:(NSString* _Nonnull)message error: (NSError* _Nonnull)err cancelHandler:(void (^ __nullable)(UIAlertAction * _Nonnull action ))cancelHandler confirmHandler:(void (^ __nullable)(UIAlertAction * _Nonnull action))confirmHandler;
+ (void)dismissGameStartAlertView;
@end
