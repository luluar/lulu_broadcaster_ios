//
//  Record.h
//  LuluBroadcaster
//
//  Created by 林威 on 17/6/19.
//  Copyright © 2017年 ShuoTan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Record : NSObject

+ (Record* _Nonnull) recordWithJSON:(id _Nonnull)data type:(NSInteger)type;

@end
