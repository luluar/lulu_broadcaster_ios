//
//  GiftRecord.h
//  LuluBroadcaster
//
//  Created by 林威 on 17/6/19.
//  Copyright © 2017年 ShuoTan. All rights reserved.
//

#import "Record.h"

@interface GiftRecord : Record

@property(nonatomic, strong, nonnull) NSString* date;
@property(nonatomic, strong, nonnull) NSString* gift_name;
@property(nonatomic, strong, nonnull) NSString* gift_cost;
@property(nonatomic, strong, nonnull) NSString* room;

+ (GiftRecord* _Nonnull) giftRecordWithJSON:(id _Nonnull)data;

@end
