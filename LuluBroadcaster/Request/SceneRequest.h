//
//  SceneRequest.h
//  LuluBroadcaster
//
//  Created by ShuoTan on 4/26/17.
//  Copyright © 2017 ShuoTan. All rights reserved.
//

#import "LuluRequest.h"
#import "Record.h"

@interface SceneRequest : LuluRequest
+ (SceneRequest* _Nullable)sharedRequest;
- (void)fetchScenesWithType:(NSInteger)type
                       Date:(NSInteger)date
                     PageNo:(NSInteger)pageNo
                 Completion: (void (^_Nullable)(NSArray*  _Nullable, NSString * _Nullable, NSError * _Nullable, NSInteger))complete;
@end
