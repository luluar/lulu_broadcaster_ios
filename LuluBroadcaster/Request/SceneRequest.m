//
//  SceneRequest.m
//  LuluBroadcaster
//
//  Created by ShuoTan on 4/26/17.
//  Copyright © 2017 ShuoTan. All rights reserved.
//

#import "SceneRequest.h"
#import "UserSession.h"

@implementation SceneRequest
+ (SceneRequest* _Nullable)sharedRequest{
    static SceneRequest *sharedMyRequest = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyRequest = [[self alloc] init];
    });
    return sharedMyRequest;
}

- (void)fetchScenesWithType:(NSInteger)type
                       Date:(NSInteger)date
                     PageNo:(NSInteger)pageNo
                 Completion: (void (^_Nullable)(NSArray*  _Nullable, NSString * _Nullable, NSError * _Nullable, NSInteger))complete
{
    NSString* broadcaster_id = [UserSession new].currentBroadcaster.room;
    NSDictionary* params = @{@"broadcaster_id": broadcaster_id, @"type":@(type), @"date":@(date), @"pageNo":@(pageNo)};
    [self postWithURL:[self phpUrlByService:@"scenes"] Parameters:params Success:^(id  _Nullable responseObject) {
        id jsonObj = responseObject[@"data"][@"record"];
        NSString *total = responseObject[@"data"][@"total"];
        NSInteger isend = [responseObject[@"data"][@"isend"] integerValue];
        if(jsonObj && [jsonObj isKindOfClass:[NSArray class]]){
            NSMutableArray* temp_array = [NSMutableArray new];
            for(id iter in jsonObj){
                Record* record = [Record recordWithJSON:iter type:type];
                [temp_array addObject:record];
            }
            complete(temp_array, total, nil, isend);
        }
        else{
            complete(@[], nil, nil, 1);
        }
        
    } Failure:^(NSError * _Nonnull error) {
        NSLog(@"error = %@", error);
        complete(@[], nil, error, 1);
    }];

}

@end
