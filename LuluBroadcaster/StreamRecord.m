//
//  StreamRecord.m
//  LuluBroadcaster
//
//  Created by 林威 on 17/6/19.
//  Copyright © 2017年 ShuoTan. All rights reserved.
//

#import "StreamRecord.h"
#import <KZPropertyMapper/KZPropertyMapper.h>

@implementation StreamRecord

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.date = @"";
        self.time = @"";
        self.cost_wealth = @"";
        self.room = @"";
    }
    return self;
}

+ (StreamRecord* _Nonnull)streamRecordWithJSON:(id _Nonnull)data{
    StreamRecord* record = [[StreamRecord alloc] init];
    [record doMappingWithData:data];
    return record;
}

- (void)doMappingWithData: (id)data{
    NSDictionary* mapping = @{@"date": KZProperty(date),
                              @"time": KZProperty(time),
                              @"room": KZProperty(room),
                              @"cost_wealth": KZProperty(cost_wealth)
                              };
    
    [KZPropertyMapper mapValuesFrom:data toInstance:self usingMapping:mapping];
}

@end
