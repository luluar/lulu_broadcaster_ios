//
//  Record.m
//  LuluBroadcaster
//
//  Created by ShuoTan on 4/26/17.
//  Copyright © 2017 ShuoTan. All rights reserved.
//

#import "LiveRecord.h"
#import <KZPropertyMapper/KZPropertyMapper.h>

@implementation LiveRecord
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.date = @"";
        self.start_time = @"";
        self.end_time = @"";
        self.room = @"";
        self.duration = @"";
    }
    return self;
}

+ (LiveRecord* _Nonnull) liveRecordWithJSON:(id _Nonnull)data{
    LiveRecord* record = [[LiveRecord alloc] init];
    [record doMappingWithData:data];
    return record;
}

- (void)doMappingWithData: (id)data{
    NSDictionary* mapping = @{@"date": KZProperty(date),
                              @"start_time": KZProperty(start_time),
                              @"end_time": KZProperty(end_time),
                              @"room": KZProperty(room),
                              @"duration": KZProperty(duration)
                              };
    
    [KZPropertyMapper mapValuesFrom:data toInstance:self usingMapping:mapping];
}
@end
