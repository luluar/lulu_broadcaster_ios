//
//  Record.h
//  LuluBroadcaster
//
//  Created by ShuoTan on 4/26/17.
//  Copyright © 2017 ShuoTan. All rights reserved.
//

#import "Record.h"

@interface LiveRecord : Record

@property(nonatomic, strong, nonnull) NSString* date;
@property(nonatomic, strong, nonnull) NSString* start_time;
@property(nonatomic, strong, nonnull) NSString* end_time;
@property(nonatomic, strong, nonnull) NSString* duration;
@property(nonatomic, strong, nonnull) NSString* room;

+ (LiveRecord* _Nonnull) liveRecordWithJSON:(id _Nonnull)data;
@end
